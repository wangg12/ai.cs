Supported spacecraft coordinates
================================

AI.CS supports the same selection of spacecraft coordinate systems as the original `CXFORM <https://spdf.sci.gsfc.nasa.gov/pub/software/old/selected_software_from_nssdc/coordinate_transform/>`_ library does:

GEI
    Geocentric Equatorial Inertial, also known as True Equator and True Equinox of Date, True of Date (TOD), ECI, or GCI

J2000
    Geocentric Equatorial Inertial for epoch J2000.0 (GEI2000), also known as Mean Equator and Mean Equinox of J2000.0

GEO
    Geographic, also known as Greenwich Rotating Coordinates (GRC), or Earth-fixed Greenwich (EFG)

MAG
    Geomagnetic

GSE
    Geocentric Solar Ecliptic

GSM
    Geocentric Solar Magnetospheric

SM
    Solar Magnetic

RTN
    Radial Tangential Normal (Earth-centered)

GSEQ
    Geocentric Solar Equatorial

HEE
    Heliocentric Earth Ecliptic

HAE
    Heliocentric Aries Ecliptic

HEEQ
    Heliocentric Earth Equatorial
