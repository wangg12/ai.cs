"""The module contains tests for ai.cs package."""
from datetime import datetime
import numpy as np
from pytest import approx
from ai import cs

def test_cart2sp():
    """Tests cart2sp function."""
    assert cs.cart2sp(1, 0, 0) == approx((1, 0, 0))
    assert cs.cart2sp(1, 1, 0) == approx((np.sqrt(2), 0, np.pi/4))
    assert cs.cart2sp(1, 1, 1) == approx(
        (np.sqrt(3), np.arctan2(1, np.sqrt(2)), np.pi/4)
    )

def test_sp2cart():
    """Tests sp2cart function."""
    assert cs.sp2cart(1, 0, 0) == approx((1, 0, 0))
    assert cs.sp2cart(np.sqrt(2), 0, np.pi/4) == approx((1, 1, 0))
    assert cs.sp2cart(
        np.sqrt(3), np.arctan2(1, np.sqrt(2)), np.pi/4
        ) == approx((1, 1, 1))

def test_cart2cyl():
    """Tests sp2cyl function."""
    assert cs.cart2cyl(1, 0, 0) == approx((1, 0, 0))
    assert cs.cart2cyl(1, 1, 0) == approx((np.sqrt(2), np.pi/4, 0))
    assert cs.cart2cyl(1, 1, 1) == approx(
        (np.sqrt(2), np.pi/4, 1)
    )

def test_cyl2cart():
    """Tests cyl2cart function."""
    assert cs.cyl2cart(1, 0, 0) == approx((1, 0, 0))
    assert cs.cyl2cart(np.sqrt(2), np.pi/4, 0) == approx((1, 1, 0))
    assert cs.cyl2cart(np.sqrt(2), np.pi/4, 1) == approx((1, 1, 1))

def test_cxform():
    """Tests cxform function."""
    assert cs.cxform(
        "J2000",
        "GEO",
        datetime(2005, 3, 2, 9, 28, 11),
        -896921337.28302002,
        220296912.43620300,
        44419205.01961136
    ) == approx((
        -664946479.875617,
        -640940392.874836,
        44869503.669364
    ))
    assert cs.cxform(
        "J2000",
        "GSE",
        datetime(2005, 3, 2, 9, 28, 11),
        -896921337.28302002,
        220296912.43620300,
        44419205.01961136
    ) == approx((
        -920802840.504282,
        -70523436.668648,
        -46046221.090855
    ))
    assert cs.cxform(
        "J2000",
        "GSM",
        datetime(2005, 3, 2, 9, 28, 11),
        -896921337.28302002,
        220296912.43620300,
        44419205.01961136
    ) == approx((
        -920802840.504282,
        -58593148.345827,
        -60503326.877360
    ))
    assert cs.cxform(
        "J2000",
        "SM",
        datetime(2005, 3, 2, 9, 28, 11),
        -896921337.28302002,
        220296912.43620300,
        44419205.01961136
    ) == approx((
        -915527671.753106,
        -58593148.345827,
        115531839.327171
    ))
    data = cs.cxform(
        "J2000",
        "SM",
        [datetime(2005, 3, 2, 9, 28, 11), datetime(2005, 3, 2, 9, 28, 11)],
        [-896921337.28302002, -896921337.28302002],
        [220296912.43620300, 220296912.43620300],
        [44419205.01961136, 44419205.01961136]
    )
    assert (data[0][0], data[1][0], data[2][0]) == approx((
        -915527671.753106,
        -58593148.345827,
        115531839.327171
    ))
    assert (data[0][1], data[1][1], data[2][1]) == approx((
        -915527671.753106,
        -58593148.345827,
        115531839.327171
    ))
