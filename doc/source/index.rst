Welcome to AI.CS
================

AI.CS is the coordinates transformation package in Python. It offers functionality for converting data between geometrical coordinates (cartesian, spherical and cylindrical) as well as between geocentric and heliocentric coordinate systems typically used in spacecraft measurements. The package currently also supports rotations of data by means of `rotation matrices <https://en.wikipedia.org/wiki/Rotation_matrix>`_. Transformations between spacecraft coordinate systems are implemented as a Python binding to the `CXFORM <https://spdf.sci.gsfc.nasa.gov/pub/software/old/selected_software_from_nssdc/coordinate_transform/>`_ library.

.. toctree::
   :maxdepth: 2
   
   getting_started
   spacecraft_coordinates
   api
